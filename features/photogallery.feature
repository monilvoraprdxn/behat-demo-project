@143
Feature: login of a user
@javascript
Scenario: abc
Given I am on "http://craind8dev.prod.acquia-sites.com/user"
Then maximize the window
Then I wait for 2 seconds
And I fill in "contentadmin" for "name"
Then I wait for 2 seconds
And I fill in "contentadmin" for "pass"
Then I wait for 5 seconds
And I press "edit-submit"
When I Hover over the element ".toolbar-icon-system-admin-content"
Then I wait for 2 seconds
When I Hover over the element ".toolbar-icon-admin-toolbar-tools-add-content"
Then I wait for 2 seconds
Then I click the ".toolbar-icon-node-add-photo-gallery" element
Then I wait for 2 seconds
And I fill in "Prdxn Photo Gallery" for "title[0][value]"
And I fill in "Photo gallery of prdxn" for "field_seo_headline[0][value]"
Then I click the "#edit-field-gallery-images-entity-browser-entity-browser-open-modal" element
Then I wait for 5 seconds
Given I switch to the iframe "entity_browser_iframe_gallery_browser"
Then I wait for 2 seconds
Then I click the ".col-1 .image-style-subscribe-thumbnail-150x180" element
Then I click the "#edit-submit" element
Then I wait for 4 seconds
Then I click the "#edit-use-selected" element
Then I wait for 4 seconds
And I fill in "category test (38536)" for "field_category[0][target_id]"
And I fill in "single topic (38396)" for "field_topics[0][target_id]"
And I fill in "Metered (1)" for "field_access_control[0][target_id]"
And I fill in "08/11/2017" for "field_pub_date[0][value][date]"
Then I wait for 2 seconds
And I fill in "00:00:00" for "field_pub_date[0][value][time]"
And I fill in "Test SV Photogallery 12/29 (2246)" for "field_related_photo_galleries[0][target_id]"
And I fill in "1234" for "field_original_id[0][value]"
Then I wait for 4 seconds
Then I click the "#edit-submit" element
Then I wait for 10 seconds