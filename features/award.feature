@143
Feature: login of a user
@javascript
Scenario: abc
Given I am on "http://craind8dev.prod.acquia-sites.com/user"
Then maximize the window
Then I wait for 2 seconds
And I fill in "contentadmin" for "name"
Then I wait for 2 seconds
And I fill in "contentadmin" for "pass"
Then I wait for 5 seconds
And I press "edit-submit"
When I Hover over the element ".toolbar-icon-system-admin-content"
Then I wait for 2 seconds
When I Hover over the element ".toolbar-icon-admin-toolbar-tools-add-content"
Then I wait for 2 seconds
Then I click the ".toolbar-icon-node-add-award-program" element
Then I wait for 2 seconds
And I fill in "Program 1 (2536)" for "field_program_name[0][target_id]"
And I fill in "2018" for "field_year[0][value]"
And I fill in "08/11/2017" for "field_publish_date[0][value][date]"
And I fill in "00:00:00" for "field_publish_date[0][value][time]"
Then I wait for 5 seconds
And I fill in "Life Achievement" for "title[0][value]"
And I fill in "dsd (32196)" for "field_status[target_id]"
And I fill in "Staff (4)" for "field_submit_nominee_link[0][uri]"
And I fill in "test" for "field_submit_nominee_link[0][title]"
Then I wait for 2 seconds
And I fill in "Staff (4)" for "field_related_event_link[0][uri]"
And I fill in "test" for "field_related_event_link[0][title]"
And I fill in "Staff (4)" for "field_faq_url[0][uri]"
And I fill in "test" for "field_faq_url[0][title]"
Then I wait for 2 seconds
And I fill in "Test organization title (2001)" for "field_recipients[0][target_id]"
And I fill in "See Details please" for "field_custom_see_details_text[0][value]"
And I fill in "Test PRDXN article (32716)" for "field_primary_sponsor[0][target_id]"
Then I wait for 5 seconds
When I select "Ascend" from "edit-field-recipients-order"
When I select "Rank" from "edit-field-recipients-order-field"
When I select "Grid" from "edit-field-recipients-listing-display"
#Then I wait for 5 seconds
When I select "Location" from "edit-field-filter-recipients"
Then I wait for 5 seconds
Then I click the "#edit-submit" element
Then I wait for 20 seconds