@143
Feature: login of a user
@javascript
Scenario: abc
Given I am on "http://craind8dev.prod.acquia-sites.com/user"
Then maximize the window
Then I wait for 2 seconds
And I fill in "contentadmin" for "name"
Then I wait for 2 seconds
And I fill in "contentadmin" for "pass"
Then I wait for 5 seconds
And I press "edit-submit"
When I Hover over the element ".toolbar-icon-system-admin-content"
Then I wait for 2 seconds
When I Hover over the element ".toolbar-icon-admin-toolbar-tools-add-content"
Then I wait for 2 seconds
Then I click the ".toolbar-icon-node-add-author" element
Then I wait for 5 seconds
#And I fill in "Ajinkya" for "title[0][value]"
#And I fill in "1234" for "field_staff_id[0][value]"
#And I fill in "Ajinkya" for "field_first_name[0][value]"
#And I fill in "Patil" for "field_last_name[0][value]"
#Then I wait for 5 seconds
#Then I click the "#edit-field-main-image-actions-ief-add-existing--j7YDxqPB-G4" element
Then I click the ".js-form-submit + .js-form-submit" element
Then I wait for 20 seconds
Then I click the ".entity-browser-processed" element
Then I wait for 2 seconds
Given I switch to the iframe "entity_browser_iframe_gallery_browser"
Then I wait for 5 seconds
Then I click the ".image-style-subscribe-thumbnail-150x180" element
Then I wait for 4 seconds
Then I click the "#edit-submit" element
Then I wait for 5 seconds
Then I click the "#edit-use-selected" element
Then I wait for 4 seconds
And I fill in "Ajpatil@prdxn.com" for "field_email[0][value]"
And I fill in "test 1 (41)" for "field_twitter[0][uri]"
And I fill in "Testing Content (36)" for "field_linkedin[0][uri]"
And I fill in "test 1 (41)" for "field_facebook[0][uri]"
And I fill in "test 1 (41)" for "field_google_plus[0][uri]"
And I fill in "test 1 (41)" for "field_instagram[0][uri]"
And I fill in "test 1 (41)" for "field_rss[0][uri]"
And I fill in "1234567890" for "field_phone[0][value]"
And I fill in "12345" for "field_fax[0][value]"
And I fill in "IT sector" for "field_department[0][value]"
And I fill in "Developer" for "field_position[0][value]"
When I select "India" from "field_address[0][address][country_code]"
And I fill in "Testtest" for "field_address[0][address][address_line1]"
And I fill in "Mumbai" for "field_address[0][address][locality]"
And I fill in "400071" for "field_address[0][address][postal_code]"
And I fill in "The living Legend" for "field_blog_label[0][value]"
And I fill in "Test" for "field_beat_details[0][value]"
And I fill in "Google" for "field_source[0][value]"
Then I click the "#edit-submit" element
Then I wait for 20 seconds