@143
Feature: login of a user
@javascript
Scenario: abc
Given I am on "http://craind8dev.prod.acquia-sites.com/user"
Then maximize the window
Then I wait for 2 seconds
And I fill in "contentadmin" for "name"
Then I wait for 2 seconds
And I fill in "contentadmin" for "pass"
Then I wait for 5 seconds
And I press "edit-submit"
When I Hover over the element ".toolbar-icon-system-admin-content"
Then I wait for 2 seconds
When I Hover over the element ".toolbar-icon-admin-toolbar-tools-add-content"
Then I wait for 2 seconds
Then I click the ".toolbar-icon-node-add-article" element
Then I wait for 5 seconds
And I fill in "Prdxn testing case" for "title[0][value]"
And I fill in "prdxn tesingg cases for crains" for "field_seo_headline[0][value]"
And I fill in "testing case" for "field_subhead[0][value]"
#Then I fill in editor field "iframe" with "This is the Summary for case"
And I fill in "Testing main points" for "field_title[0][value]"
And I fill in "Test1" for "field_summary_points[0][value]"
And I fill in "Test2" for "field_summary_points[1][value]"
And I fill in "NEW test SV" for "field_byline[0][target_id]"
And I fill in "TesterA" for "field_byline_text[0][value]"
Then I wait for 2 seconds
When I select "Photo Gallery" from "edit-field-article-emphasis"
And I fill in "Test SV Photogallery 12/29" for "field_photo_gallery[0][target_id]"
When I select "Option 3" from "edit-field-photo-gallery-layout"
Then I wait for 2 seconds
And I fill in "category test (38116)" for "field_category[0][target_id]"
And I fill in "single topic (38396)" for "field_topics[target_id]"
And I fill in "Test PRDXN article" for "field_sponsors[0][target_id]"
And I fill in "test 1" for "field_sidebar_links[0][target_id]"
Then I wait for 2 seconds
And I fill in "Test Creating Content (15556)" for "field_related_articles[0][target_id]"
And I fill in "08/11/2017" for "field_pub_date[0][value][date]"
And I fill in "00:00:00" for "field_pub_date[0][value][time]"
And I fill in "Web (Single) (2086)" for "field_access_control[0][target_id]"
Then I wait for 5 seconds
Then I click the "#edit-submit" element
Then I wait for 20 seconds


#Many entities are called Test Creating Content. Specify the one you want by appending the id in parentheses, like "Test Creating Content (15556)".
#Multiple entities match this reference; "category test (38116)", "category test (38441)", "category test (38511)". Specify the one you want by appending the id in parentheses, like "category test (38511)".
#The referenced entity (taxonomy_term: Single) does not exist.
#Headline