<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;

/**
* Defines application features from the specific context.
*/

class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {
static $options = null;
/**
* Every scenario gets its own context instance.
*
* You can also pass arbitrary arguments to the
* context constructor through behat.yml.
*/
public function __construct() {

    }
 /**
      * @Then test link url :locator
      */
     public function testlinkurl($locator)
     {
       $session = $this->getSession(); // get the mink session
       $element = $session->getPage()->find('xpath', $locator);
       $invoiceURL=$element->getAttribute('href');
        if(strpos($invoiceURL,'facebook'))
         {
          echo "Facebook Link available on page is  ".$invoiceURL;
        }
        elseif (strpos($invoiceURL,'twitter')) {
          echo "Twitter Link available on page is  ".$invoiceURL;
        }
        elseif (strpos($invoiceURL,'google')) {
          echo "Google Link available on page is  ".$invoiceURL;
        } 
        elseif (strpos($invoiceURL,'linkedin')) {
          echo "LinkedIn Link available on page is  ".$invoiceURL;
        }
        elseif (strpos($invoiceURL,'feed')!== false) {
          echo "Feed Link available on page is  ".$invoiceURL;
        }
        else {
          echo "Social link is not valid";
        }
     }

      /**
       * @Given the page title should be :arg1
       */
      public function thePageTitleShouldBe($arg1)
      {
          $actTitle = $this->getSession()->getPage()->find('css','title')->getText();

          if ($arg1=== $actTitle) {

            echo "Page Title is "."$actTitle";
          }
          else {
              throw new Exception ('Incorrect title');
          }
      }

      /**
         * @Then I wait for :time seconds
         */
        public function iWaitForSeconds($time)
        {
            sleep($time);
        }

        /**
        * @Then maximize the window
        */
        public function maximizethewindow()
        {
          $this->getSession()->resizeWindow(1440, 900, 'current');
        }


        /**
        *@When I hover the element :locator
        */
        // public function iHoverTheEelement($locator)
        // {
        //    $session=$this->getSession();
        //    try{
        //    $element= $session->getPage()->find('xpath',$locator);
        //  }
        //  catch (Exception $e){
        //    $element= $session->getPage()->find('css',$locator);
        //  }
        //  $element->mouseOver();
        //    echo "We are at ".$element->getText();
        // }

        /**
         * @When I hover over the element :arg1
         */
        public function iHoverOverTheElement($locator)
        {
                $session = $this->getSession(); // get the mink session
                $element = $session->getPage()->find('css', $locator); // runs the actual query and returns the element

                // errors must not pass silently
                if (null === $element) {
                    throw new \InvalidArgumentException(sprintf('Could not evaluate CSS selector: "%s"', $locator));
                }

                // ok, let's hover it
                $element->mouseOver();
        }




        /**
    * @Then I match the href :href of form :FormName
    */
   public function iMatchTheHrefOfForm($expectedhref,$FormName)
   {
     $actualhref = self::iFetchTheHrefOfFormName($FormName);
     $pos = strpos($actualhref, $expectedhref);
     if ($pos === false)
     {
              throw new \Exception("Expected href {$expectedhref} not found");
     }
   }
   /**
    * @Then I fetch the href of formname :FormName
    * this method is called to get the href of a particular form
    */
   public function iFetchTheHrefOfFormName($FormName)
   {
     $page = $this->getSession()->getPage();
     $actuallink = $page->findLink($FormName);
     if ($actuallink === null)
     {
       throw new Exception("The actuallink was not found!");
     }
     return $actuallink->getAttribute('href');
   }
   /**
    * @Then I fetch the href of form :FormName
    */
   public function iFetchTheHrefOfForm($FormName)
   {
     $actualhref = self::iFetchTheHrefOfFormName($FormName);
     echo "The href is: ". $actualhref;
   }
  /**
       * @Then I fetch the href of form :FormName in :region region
      */
 public function iFetchTheHrefOfFormInRegion($FormName, $region)
 {
 $actualhref = self::iFetchTheHrefOfFormNameInRegion($FormName,$region);
 echo "The href is: ". $actualhref;
 }
 public function iFetchTheHrefOfFormNameInRegion($FormName, $region)
 {
    $regionObj = $this->getSession()->getPage()->find('region', $region);
    $actuallink = $regionObj->findLink($FormName);
 if ($actuallink === null) {
     throw new Exception("The actuallink was not found!");
     }
 return $actuallink->getAttribute('href');
 }
 /**
  * @Then I match the href :href of form :FormName in :region region
  */
 public function iMatchTheHrefOfFormInRegion($expectedhref,$FormName, $region)
 {    $actualhref = self::iFetchTheHrefOfFormNameInRegion($FormName, $region);
   $pos = strpos($actualhref, $expectedhref);
         if ($pos === false) {
            throw new \Exception("Expected href {$expectedhref} not found");
 }
 }

 /**
 * @Then I click :text on locator :locator
 */
public function Searchandclick($text,$locator)
{
  $flag=0;
  try
  {
    $element = $this->getSession()->getPage()->findAll('xpath',$locator);

  } catch (Exception $e)
  {
    $element = $this->getSession()->getPage()->findAll('css',$locator);
  }

    if ($element!== null)
    {
      foreach($element as $row)
      {
        if(strpos($row->getText(),$text)!==false)
        {
          global $flag;
          $flag=1;
            $row->click();
            break;
        }
        else {
          {
            global $flag;
            $flag=0;
          }
        }

      }
      if($flag===0)
      {
        throw new \Exception("Text no available in list");
      }

    }
    else
    {
        throw new \Exception("Your are trying to click on null");
    }
}
 /**
      *@Then i should see the textbox :locator
      */
      public function ishouldseethetextbox($locator)
      {
         $session=$this->getSession();
         $element= $session->getPage()->find('xpath',$locator);
      }


     /**
       *@Then Number of :name Links are Max :max and Min :min at path :locator
       */
       public function numberoflinksaremaxandminatpath($locator,$name,$max,$min)
       {
          $session=$this->getSession();
          $element= $session->getPage()->findAll('xpath',$locator);
          if (count($element)>=$min || count($element)<=$max)
          {
            echo "Number of ".$name." Links are ".count($element);
          }
          else {
            throw new Exception("Number of links are ".count($element));
          }
       }
       /**
            * @Then I match the relative :relative url
            */
           public function imatchTheRelativeUrl($relativeurl)
           {
               $actualurl = $this->getSession()->getCurrentUrl();
               #$relativeurl   = '';
               $pos = stripos($actualurl, $relativeurl);
               if ($pos === false)
               {
                  throw new \Exception("Relative url {$relativeurl} not found");
               }
           }

           /**
                * @Then I fetch all elements under tag :tag
                */
               public function ifetchAllElemetsUnderTag($xpath)
               {
                 $session=$this->getSession();
                 $element= $session->getPage()->findAll('li',$xpath);
                 foreach($xpath as $href) {
                   echo $href->getAttribute('href')."<br />";
                   {
                      throw new \Exception("Relative url {$relativeurl} not found");
                   }
               }

             }
             /**
 * @Then I am able to see total number of options for :locator
 */
 public function ishouldseeoptions($locator)
 {
  $session=$this->getSession();
  try {
 $element= $session->getPage()->findAll('css',$locator);
  }
  catch (Exception $e) {
 $element= $session->getPage()->findAll('xpath',$locator);
  }
  echo "Number of options available are ".count($element);
      foreach($element as $row)
      {
        echo "\n".$row->getText();
        if ($row->getAttribute("href")!=null)
        {
          echo "\nhref is ".$row->getAttribute("href")."\n";
        } else if($row->getAttribute("src")!=null){
          echo "\nvalue is ".$row->getAttribute("src")."\n";
          if($row->getParent('a')->getAttribute("href")!=null) {
            echo "\nlink is ".$row->getParent('a')->getAttribute("href")."\n";
          }
        } else if($row->getAttribute("value")!=null){
          echo "\nvalue is ".$row->getAttribute("value")."\n";
        }
        else {
          echo "\nno href or value available with element"."\n";
        }
      }
 }


/**
  * @When I drag :locator1 to :locator2
  */
  public function iDragTo($locator1,$locator2){
    $dragged = $this->getSession()->getPage()->find('xpath',$locator1);
    $target = $this->getSession()->getPage()->find('xpath',$locator2);
    $dragged->dragTo($target);
  }

 /**
 * @Then I should not see CSS element :css
 *
 *
 */
 public function iShouldnotSeeCssElement($css) {
 $session = $this->getSession();
 $regionObj = $session->getPage();
 $regionText = $regionObj->find('css',$css);
 if (NULL===$regionText) {
 echo "Field did not appeared on page";
 return true;
 } else {
 throw new \Exception("Field appeared on page");
 return false;
 }
 }
 /**
 * @Then  click on :locator Content from panalizer
 */
 public function panalizerclick($locator)
 {

 try
 {
 $element = $this->getSession()->getPage()->find('xpath',$locator);

 } catch (Exception $e)
 {
 $element = $this->getSession()->getPage()->find('css',$locator);
 }
 $lock=$this->getSession()->getPage()->find('xpath',"//a[@title='Locked']");
 if($lock!==null)
 {
$lock->click();
 $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
 sleep(7);
 $element->click();
 }
 else
 {
 $element->click();
}

 }

         /**
           * @Then I should see CSS :css in the :region region
           *
           *
           */
           public function iShouldSeeCssInRegion($css, $region) {
           $session = $this->getSession();
           $regionObj = $session->getPage()->find('region', $region);

           $regionText = $regionObj->Find('css',$css);
           if (NULL===$regionText) {
              throw new \Exception(sprintf('No CSS found on the region %s.', $region, $session->getCurrentUrl()));
           }
         }

        /**
          * @Then /^I should see the css selector "([^"]*)"$/
          * @Then /^I should see the CSS selector "([^"]*)"$/
          */
         public function iShouldSeeTheCssSelector($css_selector)
         {
           $element = $this->getSession()->getPage()->find("css", $css_selector);
           if (empty($element))
           {
             throw new \Exception(sprintf("The page '%s' does not contain the css selector '%s'", $this->getSession()->getCurrentUrl(), $css_selector));
           }
         }
         /**
         * @When /^I should see the element with xpath "([^"]*)"$/
         */
         public function findxpath($xpath)
         {
           $session = $this->getSession(); // get the mink session
           $element = $session->getPage()->find('xpath', $xpath);
           if($element==null){
             throw new \Exception(' Element Not found');
           }
         }
         /**
            * @Then I should see CSS element :css
            *
            *
            */
            public function iShouldSeeCssElement($css) {
              $session = $this->getSession();
              $regionObj = $session->getPage();
              $regionText = $regionObj->find('css',$css);
              if (NULL===$regionText) {
                 throw new \Exception("Field did not appeared on page");
                return false;
              } else {
                echo "Field appeared on page";
                return true;
              }
            }

            /**
        * @Then I click on first option on page for :locator
        */
        public function getfirstoptionandclick($locator)
        {
        try
        {
        $element= $this->getSession()->getPage()->findAll('xpath',$locator);
        } catch (Exception $e)
        {
        $element= $this->getSession()->getPage()->findAll('css',$locator);
        }
        foreach ($element as $value) {
        global $options;
        $options=$value->getText();
        $value->click();
        break;
        }
        }

        /**
* @Then I verify the page title :locator
*/
public function verifytitle($locator)
{
  try {
$actTitle = $this->getSession()->getPage()->find('css',$locator)->getText();
  } catch (Exception $e) {
$actTitle = $this->getSession()->getPage()->find('xpath',$locator)->getText();
  }
global $options;
if (strcasecmp($options,$actTitle)===0) {
echo "Page Title is "."$actTitle";
}
else {
throw new Exception ('Incorrect title');
}
}
            /**
              * Click on the element with the provided xpath query
              *
              * @When /^I click on the element with xpath "([^"]*)"$/
              */
             public function iClickOnTheElementWithXPath($xpath)
             {
                 $session = $this->getSession(); // get the mink session
                 $element = $session->getPage()->find(
                     'xpath',
                     $session->getSelectorsHandler()->selectorToXpath('xpath', $xpath)
                 ); // runs the actual query and returns the element
                 // errors must not pass silently
                 if (null === $element) {
                     throw new \InvalidArgumentException(sprintf('Could not evaluate XPath: "%s"', $xpath));
                 }

                 // ok, let's click on it
                 $element->click();

             }

             /**
         *@When I click on :name element :locator
         */
         public function iclickonxpath($locator)
         {
           try {
             $element = $this->getSession()->getPage()->find('xpath',$locator);
           } catch (Exception $e) {
             $element = $this->getSession()->getPage()->find('css',$locator);
           }
            $element->click();
         }
         /**
         * @Given for locator :locator I enter :text
         */
         public function entervalueinxpath($locator,$text)
         {
          try
          {
            $element = $this->getSession()->getPage()->find('xpath',$locator);

          } catch (Exception $e)
          {
            $element = $this->getSession()->getPage()->find('css',$locator);
          }

            if ($element=== null) {

              throw new \Exception("Element not found");
            }
            else {
                $element->setValue($text);
            }
         }

         /**
            * @Given /^I set browser window size to "([^"]*)" x "([^"]*)"$/
            */
           public function iSetBrowserWindowSizeToX($width, $height) {
             $this->getSession()->resizeWindow((int)$width, (int)$height, 'current');
           }

         /**
         * Click on the element with the provided CSS Selector
         *
         * @When /^I click on the element with css selector "([^"]*)"$/
         */
          public function iClickOnTheElementWithCSSSelector($cssSelector)
          {
              $session = $this->getSession();
              $element = $session->getPage()->find(
                  'xpath',
                  $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
              );
              if (null === $element) {
                  throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
              }

              $element->click();
          }

          /**
          * @Then I click the :arg1 element
          */
           public function iClickOnElement($element)
           { $page = $this->getSession()->getPage();
              $findName = $page->find("css", $element); if (!$findName)
              {
                throw new Exception($element . " could not be found");
              }
              else { $findName->click();
              }
            }

              /**
  * @Then I fill in editor field :locator with :value
  */
 public function iFillInEditorFieldWith($locator, $value) {
   $el = $this->getSession()->getPage()->findField($locator);

   if (empty($el)) {
     throw new ExpectationException('Could not find editor with locator: ' . $locator, $this->getSession());
   }

   $fieldId = $el->getAttribute('id');

   if (empty($fieldId)) {
     throw new Exception('Could not find an id for field with locator: ' . $locator);
   }

   $this->getSession()
     ->executeScript("CKEDITOR.instances[\"$fieldId\"].setData(\"$value\");");
 }

 /**
  * @Then I should see the Tag :tag with Attribute1 :attribute1 and Attribute2 :attribute2 with value as :value
  */
   public function verifyattribute($tag,$attribute1,$attribute2,$value)
   {
     $element = $this->getSession()->getPage()->findAll('xpath',$tag);
   $flag=0;
   foreach($element as $row)
   {
   if($row->getAttribute($attribute1)===$value)
   {
   global $flag;
   $flag=1;;
   echo $row->getAttribute($attribute1)."----Value--".$row->getAttribute($attribute2);
   break;
   }
   else {
     global $flag;
   $flag=0;
   }
   }
   if($flag===0)
   {
     echo "Not match";
   }
   }

   /**
  * @When I fill in the autocomplete :autocomplete with :text and click :link
  */
 public function fillInDrupalAutocomplete($field, $text, $link) {
   $session = $this->getSession();
   $element = $session->getPage()->findField($field);
   if (empty($element)) {
     throw new ElementNotFoundException($session, NULL, 'named', $field);
   }
   $element->setValue($text);
   $element->focus();
   $xpath = $element->getXpath();
   $driver = $session->getDriver();
   $driver->keyDown($xpath, 40);
   $driver->keyUp($xpath, 40);
   sleep(5);
   $available_autocompletes = $this->getSession()->getPage()->findAll('css', 'ul.ui-autocomplete[id^=ui-id]');
   if (empty($available_autocompletes)) {
     throw new \Exception('Could not find the autocomplete popup box');
   }

   foreach ($available_autocompletes as $autocomplete) {
     if ($autocomplete->isVisible()) {
       $matched_element = $autocomplete->find('xpath', "//a[contains(.,'${link}')]");
       if (!empty($matched_element)) {
         $matched_element->click();

       }
     }
   }

 }

   /**
    * @Then I fetch the first available option for :locator
    */
   public function getfirstoptiontext($locator)
   {
     try
     {
       $element = $this->getSession()->getPage()->find('xpath',$locator);
     } catch (Exception $e)
     {
       $element = $this->getSession()->getPage()->find('css',$locator);
     }
     try {
       global $options;
       $value=$element->getAttribute('href');
      $options=$element->getText()." (".substr($value,self::strrevpos($value,'/')+strlen('/')).")";
     } catch (Exception $e) {
     }
     echo $options;
   }
   /**
    * @Given I enter the value at :locator
    */
   public function entervalue($locator)
   {
     try
     {
       $element = $this->getSession()->getPage()->find('xpath',$locator);
     } catch (Exception $e)
     {
       $element = $this->getSession()->getPage()->find('css',$locator);
     }
       if ($element=== null) {
         throw new \Exception("Element not found");
       }
       else {
      global $options;
           $element->setValue($options);
       }
   }

    public function strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
}





/**
* @Then I am able to see Articles property as :locator
*/
public function articleProperty($locator)
    {
      $session=$this->getSession();
      try {
    $row= $session->getPage()->find('css',$locator);
      }
      catch (Exception $e) {
    $row= $session->getPage()->find('xpath',$locator);
      }
    if($row->find('xpath','/div[3]//h2'))
    {
      echo "\nArticle Name is\n".$row->find('xpath','div[3]//h2')->getText()."\n";
    }
    else
    {
      echo "Article not available\n";
    }
    if($row->find('xpath','/div[2]//span'))
    {
    echo "\nTime stamp available\n";
    echo $row->find('xpath','//span[1]')->getText();
    echo "\n".$row->find('xpath','//span[2]')->getText()."\n";
    }
    else
    {
      echo "time stamp not available\n";
    }
    if($row->find('xpath',"/div[contains(@class,'block block-ctools-block block-entity-fieldnodefield-main-image')]//h2"))
    {
      echo "\nImage Name is\n".$row->find('xpath',"//div[contains(@class,'block block-ctools-block block-entity-fieldnodefield-main-image')]//h2")->getText();
    if($row->find('xpath',"/div[contains(@class,'block block-ctools-block block-entity-fieldnodefield-main-image')]//a"))
    {
      echo "\nImage is also available and Url is \n";
      echo $row->find('xpath',"/div[contains(@class,'block block-ctools-block block-entity-fieldnodefield-main-image')]//a")->getAttribute('href')."\n";
    }
    }
    else
    {
      echo "image name and image not available\n";
    }
    if($row->find('xpath',"//div[contains(@class,'article-sub-title field__item')]"))
    {
      echo "\nTaxonomy available as\n".$row->find('xpath',"//div[contains(@class,'article-sub-title field__item')]")->getText()."\n";
    }
    else
    {
      echo "Taxonomy not available\n";
    }
    }

  /**
* @Then :locator field should contain text :text
*/
public function field_contains($locator,$text)
{

 try
 {
   $element = $this->getSession()->getPage()->find('xpath',$locator);

 } catch (Exception $e)
 {
   $element = $this->getSession()->getPage()->find('css',$locator);
 }

   if ($element=== null) {

     throw new \Exception("Element not found");
   }
   else if (strpos($element->getAttribute('value'),$text)!== false)
   {
     echo "Text Found";
   }
   else
   {
       throw new \Exception("Text not matched");
   }
}

/**
  * @Given /^I switch to the iframe "([^"]*)"$/
  */
  public function iSwitchToIframe($selector)
  {
    $this->getSession()->switchToIFrame($selector);
  }

/**
 * @When /^I switch window$/
 */
  public function windowSwitch()
  {
   $windowNames = $this->getSession()->getWindowNames();
   if(count($windowNames) > 1) {
     $this->getSession()->switchToWindow($windowNames[1]);
   }
   else {
     throw new \Exception(sprintf('There is no new window for social icon'));
   }
  }

  /**
 * @Then I set main window name
 */
  public function iSetMainWindowName()
  {
      $window_name = 'main_window';
      $script = 'window.name = "' . $window_name . '"';
      $this->getSession()->executeScript($script);
  }

  /**
   * @Then I switch back to main window
   */
  public function iSwitchBackToMainWindow()
  {
      $this->getSession()->switchToWindow('main_window');
  }

  /**
   * @Then I should see( the image) :image in the :locator
   *
   * @throws \Exception
   *   If locator or image within it cannot be found.
   */
  public function assertRegionImage($image, $locator) {
    $session = $this->getSession();

    try {
      $regionObj = $session->getPage()->find('css',$locator);
    } 
    catch (Exception $e) {
      $regionObj = $session->getPage()->find('xpath',$locator);
    }

    $regionImage = $regionObj->getAttribute('src');
    if (strpos($regionImage, $image) === FALSE) {
      throw new \Exception(sprintf("The Image '%s' was not found in the region '%s' on the page %s", $image, $locator, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then I should see the image in the :locator
   *
   * @throws \Exception
   *   If locator within it cannot be found.
   */
  public function assertRegionImages($locator) {
    $session = $this->getSession();

    try {
      $regionObj = $session->getPage()->findAll('css',$locator);
    } 
    catch (Exception $e) {
      $regionObj = $session->getPage()->findAll('xpath',$locator);
    }

    if(count($regionObj) < 2) {
      throw new \Exception(sprintf("The Image was not found on the page %s", count($regionObj), $locator, $this->getSession()->getCurrentUrl()));
    } 
  }

  /**
   * Return a URL after @ character.
   *
   * @param string $url
   */
  public function fetchUrl($url = '') {
    if (($pos = strpos($url, "@")) !== FALSE) { 
      $url = substr($url, $pos+1); 
    }
    return $url;
  }

  /**
   * @Then I match the current page URL :locator element
   */
   public function imatchTheCurrentPageUrlElement($locator)
   {
      $session = $this->getSession();
      $windowNames = $session->getWindowNames();
      if(count($windowNames) > 1) {
        $session->switchToWindow('main_window');
        $actualurl = $session->getCurrentUrl();
        // Remove @ character from URL
        $actualurl = $this->fetchUrl($actualurl);
        $session->switchToWindow($windowNames[1]);
      } else {
        $session->back();
        $actualurl = $session->getCurrentUrl();
        // Remove @ character from URL
        $actualurl = $this->fetchUrl($actualurl);
        $this->getSession()->forward();
      }
      
      try
    {
      $textField = $session->getPage()->find('css',$locator);
    } 
    catch (Exception $e) {
      $textField = $session->getPage()->find('xpath',$locator);
    }

        // Find the text within the region
      $field= $textField->getText();

      if (strpos($field, $actualurl) === FALSE)
      {
        throw new \Exception("Page share url {$actualurl} not found in the {$field}");
      }
   }

  //  public function imatchTheCurrentPageUrlElement($locator)
  //  {
  //     $session = $this->getSession();
  //     $windowNames = $session->getWindowNames();
  //     if(count($windowNames) > 1) {
  //       $session->switchToWindow('main_window');
  //       $actualurl = $session->getCurrentUrl();
  //       if (($pos = strpos($actualurl, "@")) !== FALSE) { 
  //         $actualurl = substr($actualurl, $pos+1); 
  //       }
  //       $session->switchToWindow($windowNames[1]);
  //     } else {
  //       $session->back();
  //       $actualurl = $session->getCurrentUrl();
  //       if (($pos = strpos($actualurl, "@")) !== FALSE) { 
  //         $actualurl = substr($actualurl, $pos+1); 
  //       }
  //       $this->getSession()->forward();
  //     }
      
  //     $textField = $session->getPage()->find('css',$locator);

  //       // Find the text within the region
  //     $field= $textField->getText();

  //     if (strpos($field, $actualurl) === FALSE)
  //     {
  //       throw new \Exception("Page share url {$actualurl} not found in the {$field}");
  //     }
  //  }

   /**
   * @Then I match the current page domain :locator element
   */
   public function imatchTheCurrentPageDomainElement($locator)
   {

      $session = $this->getSession();
      $windowNames = $session->getWindowNames();
      if(count($windowNames) > 1) {
        $session->switchToWindow('main_window');
        $actualurl = $session->getCurrentUrl();
        // Remove @ character from URL
        $actualurl = $this->fetchUrl($actualurl);
        $arrURL = explode("/", $actualurl, 2);
        $actualurl = $arrURL[0];
        $session->switchToWindow($windowNames[1]);
      } else {
        $session->back();
        $actualurl = $session->getCurrentUrl();
        // Remove @ character from URL
        $actualurl = $this->fetchUrl($actualurl);
        $arrURL = explode("/", $actualurl, 2);
        $actualurl = $arrURL[0];
        $this->getSession()->forward();
      }
      
      try
    {
      $textField = $session->getPage()->find('css',$locator);
    } 
    catch (Exception $e) {
      $textField = $session->getPage()->find('xpath',$locator);
    }

        // Find the text within the region
      $field= $textField->getText();

      if (strpos($field, $actualurl) === FALSE)
      {
        throw new \Exception("Page share url {$actualurl} not found in the {$field}");
      }
   }

   // public function imatchTheCurrentPageDomainElement($locator)
   // {
   //    $session = $this->getSession();
   //    $session->switchToWindow('main_window');
   //    $actualurl = $session->getCurrentUrl();
   //    if (($pos = strpos($actualurl, "@")) !== FALSE) { 
   //      $actualurl = substr($actualurl, $pos+1); 
   //      $arrURL = explode("/", $actualurl, 2);
   //      $actualurl = $arrURL[0];
   //    }

   //    $windowNames = $session->getWindowNames();
   //    if(count($windowNames) > 1) {
   //      $session->switchToWindow($windowNames[1]);
   //    } else {
   //      throw new \Exception(sprintf('There is no new window for social icon'));
   //    }
      
   //    $textField = $session->getPage()->find('css',$locator);

   //      // Find the text within the region
   //    $field= $textField->getText();

   //    if (strpos($field, $actualurl) === FALSE)
   //    {
   //      throw new \Exception("Page share url {$actualurl} not found in the {$field}");
   //    }
   // }

  /**
   * @Then /^(?:|I )close the current (tab|window)$/
   */
  public function closeCurrentWindow()
  {
    $this->getSession()->executeScript("window.open('','_self').close();");
  }

  

}